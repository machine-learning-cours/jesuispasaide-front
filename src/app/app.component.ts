import { Component } from '@angular/core';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {ApiIaService, EnumDate} from "./api-ia.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private fb: FormBuilder,
    private apiIaService :ApiIaService
  ) {
  }

  form = this.fb.group({
    date: this.fb.control("", Validators.required),
    time: this.fb.control( '', [Validators.required, Validators.pattern("^([01]?[0-9]|2[0-3]):[0-5][0-9]$")]),
  })

  prediction = ""

  get date() {
    return this.form.get('date') as FormControl
  }

  get time() {
    return this.form.get('time') as FormControl
  }

  onSubmit() {
    this.apiIaService.predict(this.date.value, this.time.value).subscribe({
      next: (result) => this.prediction = result.prediction,
      error: () => console.log("error serveur")
    })
  }

  protected readonly EnumDate = EnumDate;
  protected readonly Object = Object;
}
