import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

export enum EnumDate {
  MONDAY = "Monday",
  TUESDAY = "Tuesday",
  WEDNESDAY = "Wednesday",
  THURSDAY = "Thursday",
  FRIDAY = "Friday",
  SATURDAY = "Saturday",
  SUNDAY = "Sunday"
}

@Injectable({
  providedIn: 'root'
})
export class ApiIaService {

  constructor(
    private httpClient: HttpClient
  ) { }


  predict(date: EnumDate, time: String) : Observable<{prediction: string}> {
    return this.httpClient.post<{prediction: string}>('http://localhost:8000/predict', {DayOfWeek: date, Hour: time.slice(0,2)})
  }
}
