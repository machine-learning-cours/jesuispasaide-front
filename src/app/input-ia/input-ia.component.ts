import {Component, Input} from '@angular/core';
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-input-ia',
  templateUrl: './input-ia.component.html',
  styleUrls: ['./input-ia.component.scss']
})
export class InputIaComponent {

  @Input() label = ''
  @Input() control: FormControl = {} as FormControl

}
